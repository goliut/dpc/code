Contact using [Jami](jami.net): `goliut-dpc`
Donate using [Tether](tether.to): `TVYiwCEYgES8ghNcfLpFNYAjgJizPUKPJW`

# For what?
**Example #1**: You have configured Manjaro with i3 on a laptop and want to do the same on an Ubuntu computer. You run a python script on Manjaro, transfer the archive and run another script on Ubuntu. Voila, everything is the same.

**Example #2**: You are publishing your arch setup results to r/unixporn, and you want to make sure everyone can do it without too much effort. You create an archive using a script and attach it to the post.

# Exploitation
Download and run the "make-dpc.py" script.
```
wget 'https://raw.githubusercontent.com/goliut/dpc/main/source/make-dpc.py'; python make-dpc.py
```
![Diagram](source/diagram.png)
Transfer the resulting archive using a flash drive or the Internet to another distribution / pc.

Unpack and run the nested bash script.
```
tar xvf dpc-archive.tgz; cd dpc-files; bash BASHME.sh
```
